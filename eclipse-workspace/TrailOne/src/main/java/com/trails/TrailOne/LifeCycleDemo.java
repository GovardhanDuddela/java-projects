package com.trails.TrailOne;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class LifeCycleDemo {
	@PostConstruct
    public void init()
    {
        System.out.println("Init method");
    }
private int message;

 

public int getMessage() {
    return message;
}

 

public void setMessage(int message) {
    this.message = message;
}
@PreDestroy
public void destroy()
{
    System.out.println("destroy");
}

 

}
 







