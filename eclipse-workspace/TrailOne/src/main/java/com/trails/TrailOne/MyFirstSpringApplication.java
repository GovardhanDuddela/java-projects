package com.trails.TrailOne;

import java.beans.Statement;

import org.springframework.context.ApplicationContext;

public class MyFirstSpringApplication {//eager loading and lazy loading
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext();
        Statement st=context.getBean("st",Statement.class);
        System.out.println(((Object) st).getId()+" "+st.getMethodName()+" "+st.getClass().getCity());
        System.out.println(st.getClass().getId());
    }
}