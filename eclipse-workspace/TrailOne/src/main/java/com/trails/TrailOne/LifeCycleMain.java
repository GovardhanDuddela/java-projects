package com.trails.TrailOne;
import org.springframework.context.ApplicationContext;

import com.trails.TrailOne.ClassPathXmlApplicationContext;
import com.trails.TrailOne.LifeCycleDemo;

public class LifeCycleMain {
public static void main(String[] args) {
    ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
    LifeCycleDemo lcd=context.getBean("lifecycle",LifeCycleDemo.class);
    System.out.println(lcd.getMessage());
}
}