package CarInventory.Services;

class Carsused {
	int id;
	String model;
	int year;
	float sales_price;

	public Carsused(int id, String model, int year, float sales_price) {
		this.id = id;
		this.model = model;
		this.year = year;
		this.sales_price = sales_price;
	}

	public int getid() {
		return id;
	}

	public String getModel() {
		return model;
	}

	public int getYear() {
		return year;
	}

	public float getSales_price() {
		return sales_price;
	}

@Override
public String toString() {
	return "Usedcars [model=" + model + ", year=" + year + ", sales_price=+\" $\"+" + sales_price + ", id=" + id + "]";
}


}