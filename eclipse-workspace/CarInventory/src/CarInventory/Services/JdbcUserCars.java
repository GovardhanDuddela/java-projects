package CarInventory.Services;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.InputMismatchException;
import java.util.Scanner;

import CarInventory.Util.ConnectionUtil;

public class JdbcUserCars {

	private static Statement st;

	public JdbcUserCars() throws SQLException {
		super();
		this.st = ConnectionUtil.getStatement();
	}

	public static void main(String[] args) throws SQLException {
		JdbcUserCars cars = new JdbcUserCars();
		// Statement st = ConnectionUtil.getStatement();

		try {

			// st.execute("create table Carsused(id int NOT NULL PRIMARY KEY,model
			// varchar(30),year int,sales_price varchar(30))");
			// int noOfRows=st.executeUpdate("insert into cardetails
			// System.out.println(noOfRows+"row inserted");
			// scanner class to read input
			Scanner sc = new Scanner(System.in);
			// printing services available
			print_services();
			// reading commands until user enter quit commands
			while (true) {
				System.out.print("Enter command : ");
				String command = sc.nextLine();
				if (command.equals("quit")) {
					break;
				} else {
					ResultSet rs = st.executeQuery("select count(*) from Carsused");
					rs.next();
					int count = rs.getInt(1);
					get_commands(command, count);
					print_services_old();
				}
			}
			System.out.println("Thank You!");

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ConnectionUtil.closeConnection(ConnectionUtil.getConnection());
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private static void print_services() {
		System.out.println("Hello!");
		System.out.println("Welcome to Mullet Joe's Gently Used Autos!");
		print_services_old();
	}

	/**
	 * existing print services
	 */
	private static void print_services_old() {
		System.out.println("These are the commands i can assist ");
		System.out.println("1.add - add a car to the catalog");
		System.out.println("2.list - list all cars in the catalog");
		System.out.println("3.upate- update cars in catlog");
		System.out.println("4.delete - delete all cars in the catalog");
		System.out.println("5.quit - quit the application");
	}

	private static void get_commands(String command, int count) {
		/*
		 * param command dtype-String
		 */
		if (command.equals("add")) {
			// try and catch for input mismatch exception
			try {
				// check catalog size before adding details as we limit up to 20 records
				if (count < 20) {
					// reading id,Model,Year and Sale Price of Car from user
					Scanner sc = new Scanner(System.in);
					System.out.println("id : ");
					int id = sc.nextInt();
					sc.nextLine();
					System.out.println("Model : ");
					String model = sc.nextLine();
					System.out.println("Year : ");
					int year = sc.nextInt();
					System.out.println("Sales Price ($) :");
					float sales_price = sc.nextFloat();
					// passing the parameters to CarDetails Class and creating an object
					Carsused cd = new Carsused(id, model, year, sales_price);

					String aw = "INSERT INTO Carsused (id, model,year,sales_price)" + "values(" + id + ",'"
							+ cd.getModel() + "'," + cd.getYear() + "," + cd.getSales_price() + ")";
					int noOfRows = 0;
					try {
						noOfRows = st.executeUpdate(aw);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						// e.printStackTrace();
						System.out.println(e.getMessage());
					}
					System.out.println(noOfRows + "row inserted!! " + "Inserted object " + cd.toString());
				} else {
					System.out.println("Limit Reached!!");
				}

			} catch (InputMismatchException e) {
				System.out.println("Please enter valid Details");
			}

		} else if (command.equals("list")) {
			// checking catalog is empty of not
			if (count == 0) {
				System.out.println("There are currently no cars in the catalog.");
			} else {
				int total = 0;
				String query = "select * from Carsused";
				ResultSet rs = null;
				try {
					rs = st.executeQuery(query);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					while (rs.next()) {
						int id = rs.getInt("id");
						String model = rs.getString("model");
						int year = Integer.parseInt(rs.getString("year"));
						int sales = (int) (rs.getFloat("sales_price"));
						total += sales;
						System.out.println(id + "  " + year + "  " + model + "  $" + sales);
					}
				} catch (NumberFormatException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("Number of cars " + count);
				System.out.println("Total inventory :  $" + total);
			}
		}

		else if (command.equals("delete")) {
			Scanner sc = new Scanner(System.in);
			System.out.println("enter id to delete from the list");
			int delete_id = sc.nextInt();
			try {
				st.executeUpdate("delete from Carsused where id=" + delete_id);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Record deleted successfully");
		} else if (command.equals("update")) {
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter the id of the car to update");
			int id_update = sc.nextInt();
			System.out.println("Enter the column_name of the car to update");
			String column_name = sc.next();
			if (column_name.equals("model")) {
				System.out.println("Enter the new model of the car to update");
				String new_model = sc.next();
				String str = "update Carsused set model= ? where id = ?";
				PreparedStatement stmt;
				try {
					stmt = ConnectionUtil.getPreparedStatement(str);
					stmt.setString(1, new_model);
					stmt.setInt(2, id_update);
					int noOfRowsUpdated = stmt.executeUpdate();
					System.out.println(noOfRowsUpdated + " rows updated");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else if (column_name.equals("year")) {
				System.out.println("Enter the new year of the car to update");
				int new_year = sc.nextInt();
				String str = "update Carsused set year= ? where id = ?";
				PreparedStatement stmt;
				try {
					stmt = ConnectionUtil.getPreparedStatement(str);
					stmt.setInt(1, new_year);
					stmt.setInt(2, id_update);
					int noOfRowsUpdated = stmt.executeUpdate();
					System.out.println(noOfRowsUpdated + " rows updated");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else if (column_name.equals("sales_price")) {
				System.out.println("Enter the new price of the car to update");
				float new_price = sc.nextFloat();
				String str = "update Carsused set sales_price= ? where id = ?";
				PreparedStatement stmt;
				try {
					stmt = ConnectionUtil.getPreparedStatement(str);
					stmt.setFloat(1, new_price);
					stmt.setInt(2, id_update);
					int noOfRowsUpdated = stmt.executeUpdate();
					System.out.println(noOfRowsUpdated + " rows updated");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				System.out.println("Sorry, but " + command + " is not a valid command. Please try again.");
			}

		}
	}
}
