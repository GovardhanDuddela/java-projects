package CarInventory.Util;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ConnectionUtil {

	// get statement method
	public static Statement getStatement() throws SQLException {
		Statement st = null;
		try {
			Connection con = getConnection();
			st = con.createStatement();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return st;
	}

	// Get prepared statement method
	public static PreparedStatement getPreparedStatement(String str) throws SQLException {
		PreparedStatement st = null;
		try {
			Connection con = getConnection();
			st = con.prepareStatement(str);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return st;
	}

	/**
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static Connection getConnection() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		Connection con;
		con = DriverManager.getConnection("jdbc:mysql://localhost:3306/govardhan", "root", "root");
		return con;
	}

	/**
	 * @param con
	 * @throws SQLException
	 */
	public static void closeConnection(Connection con) throws SQLException {
		con.close();
	}
}
