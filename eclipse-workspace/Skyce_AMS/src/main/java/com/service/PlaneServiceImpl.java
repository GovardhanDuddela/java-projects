package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.PlaneDAO;
import com.model.Plane;

@Service
public class PlaneServiceImpl implements PlaneService{
	@Autowired
	PlaneDAO doa;
	
	public int deletePlane(int planeNo) {
		return doa.deletePlane(planeNo);
	}
	public List<Plane> listPlanes(){
		return doa.listPlanes();
	}
	
	public Plane getPlane(int planeNo) {
		return doa.getPlane(planeNo);
	}
	public Plane savePlane(Plane plane) {
		return doa.savePlane(plane);
	}
	public Plane updatePlane(Plane plane) {
		return doa.updatePlane(plane);
	}
	

}
